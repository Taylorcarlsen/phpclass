<?php
/*
 * This is a countdown timer
 * Burning Man 8-25-3030
 */

$secPerMin = 60;
$secPerHour = 60 * $secPerMin;
$secPerDay = 24 * $secPerHour;
//$secPerYear = 365 * $secPerDay;

//current time
$now = time();

//burning man time
$semesterEnd = mktime(12,0,0,12,19,2018);

//how many seconds between now and then
$seconds = $semesterEnd - $now;

//$Years = floor($seconds/$secPerYear);
//$seconds = $seconds - ($Years * $secPerYear);

$Days = floor($seconds / $secPerDay);
$seconds = $seconds - ($Days * $secPerDay);

$Hours = floor($seconds / $secPerHour);
$seconds = $seconds - ($Hours * $secPerHour);

$Minutes = floor($seconds / $secPerMin);
$seconds = $seconds - ($Minutes * $secPerMin);


?>

<!doctype html>
<html language="en">
<head>
    <meta charset="UTF-8">
    <title>Taylor's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h3>Semester End Countdown</h3>
    <p>Days: <?=$Days?> | Hours: <?=$Hours?> | Minutes: <?=$Minutes?> | Seconds: <?=$seconds?></p>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
