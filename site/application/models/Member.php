<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Model {

	public function user_login($email,$pass)
    {
        $this->load->database();
        $this->load->library('session');

        try{
            $db = new PDO($this->db->dsn, $this->db->username, $this->db->password, $this->db->options);
            $sql = $db->prepare("select memberID, memberPassword, memberKey from memberLogin where memberEmail = :Email and roleID = 2");
            $sql->bindValue(":Email",$email);
            $sql->execute();
            $row = $sql->fetch();

            if($row != null){

                $hashedPassword = md5($pass . $row["memberKey"]);
                echo $hashedPassword;

                if($hashedPassword == $row["memberPassword"]){
                    $this->session->set_userdata(array("UID"=>$row["memberID"]));
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }

        }catch (PDOException $e){
            $error = $e->getMessage();
            echo "Error: $error";
        }

    }
    public function user_create($fname,$user_name,$password)
    {
        $this->load->database();
        //create guid
        $key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
        $role = 2;
        try {
            $db = new PDO($this->db->dsn, $this->db->username, $this->db->password, $this->db->options);
            $sql = $db->prepare("insert into memberLogin (memberName,memberEmail,memberPassword,roleID,memberKey) VALUE(:Name,:Email,:Password,:Role,:Key)");
            $sql->bindValue(":Name", $fname);
            $sql->bindValue(":Email", $user_name);
            $sql->bindValue(":Password", md5($password . $key));
            $sql->bindValue(":Role",$role);
            $sql->bindValue(":Key", $key);
            $sql->execute();
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
}
