<?php
include '../includes/dbConn.php';

if(isset($_POST["txtTitle"])){
    if(isset($_POST["txtRating"]))
    {
        $title = $_POST["txtTitle"];
        $rating = $_POST["txtRating"];
        $id = $_POST["txtID"];

        //database stuff
        try{
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("update movielist set movieTitle = :Title, movieRating = :Rating where movieID = :ID");
            $sql->bindValue(":Title",$title);
            $sql->bindValue(":Rating",$rating);
            $sql->bindValue(":ID",$id);
            $sql->execute();
        }catch (PDOException $e){
            $error = $e->getMessage();
            echo "Error: $error";
        }

        header("Location:movielist.php");
    }
}

if(isset($_GET["id"])){
    $id=$_GET["id"];
    try{
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("select * from movielist where movieID = :id");
        $sql->bindValue(":id",$id);
        $sql->execute();
        $row = $sql->fetch();               //fetch values from the db
        $rating = $row["movieRating"];      //put values into the UI table from the db
        $title = $row["movieTitle"];
    }catch (PDOException $e){
        $error = $e->getMessage();
        echo "Error: $error";
    }
}else{
    header("Location:movielist.php");
}

?>

<!doctype html>
<html language="en">
<head>
    <meta charset="UTF-8">
    <title>Taylor's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <script type="text/javascript">
        function DeleteMovie(title,id) {
            if(confirm("Do you want to delete: " + title)){
                document.location.href = "moviedelete.php?id=" + id;
            }
        }
    </script>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <form method="post">
        <table border = "1" width = "80%"">
            <tr height = "50">
                <th colspan="2">Update Movie</th>
            </tr>
            <tr height = "40">
                <th>Movie Name</th>
                <td><input type="text" size="40" id="txtTitle" name="txtTitle" value="<?=$title?>"></td>
            </tr>
            <tr height = "40">
                <th>Movie Rating</th>
                <td><input type="text" size="40" id="txtRating" name="txtRating" value="<?=$rating?>"></td>
            </tr>
            <tr height = "50">
                <th colspan="2">
                    <input type="submit" value="Update Movie"> | <input type="button" onclick="DeleteMovie('<?=$title?>','<?=$id?>')" value="Delete Movie">
                </th>
            </tr>
        </table>
        <input type="hidden" id="txtID" name="txtID" value="<?=$id?>">
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>