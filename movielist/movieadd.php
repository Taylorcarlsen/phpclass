<?php

if(isset($_POST["txtTitle"])){
    if(isset($_POST["txtRating"]))
    {
        $title = $_POST["txtTitle"];
        $rating = $_POST["txtRating"];

        //database stuff
        include '../includes/dbConn.php';
        try{
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("insert into movielist (movieTitle,movieRating) VALUE(:Title,:Rating)");
            $sql->bindValue(":Title",$title);
            $sql->bindValue(":Rating",$rating);
            $sql->execute();
        }catch (PDOException $e){
            $error = $e->getMessage();
            echo "Error: $error";
        }

        header("Location:movielist.php");
    }
}

?>

<!doctype html>
<html language="en">
<head>
    <meta charset="UTF-8">
    <title>Taylor's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <form method="post">
        <table border = "1" width = "80%"">
            <tr height = "50">
                <th colspan="2">Add New Movie</th>
            </tr>
            <tr height = "40">
                <th>Movie Name</th>
                <td><input type="text" size="40" id="txtTitle" name="txtTitle"></td>
            </tr>
            <tr height = "40">
                <th>Movie Rating</th>
                <td><input type="text" size="40" id="txtRating" name="txtRating"></td>
            </tr>
            <tr height = "50">
                <th colspan="2"><input type="submit" value="Add New Movie"></th>
            </tr>
        </table>
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>