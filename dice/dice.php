<?php

    //dice values 1 to 6
    $diceValues = array();
    /*$diceValues[0] = 1;
    $diceValues[1] = 2;
    $diceValues[2] = 3;
    $diceValues[3] = 4;
    $diceValues[4] = 5;
    $diceValues[5] = 6;*/
    for($i=0;$i<=5;$i++){
        $diceValues[$i] = $i+1;
    }

    /*$iDice = array();

    for($i=0;$i<=6;$i++) {
        $iDice[$i] = $iDice.$i+1;
        $iDice[$i] = mt_ran(0, 5);
    }*/

    //six randomly rolled die
    $iDice1 = mt_rand(0,5);
    $iDice2 = mt_rand(0,5);
    $iDice3 = mt_rand(0,5);
    $iDice4 = mt_rand(0,5);
    $iDice5 = mt_rand(0,5);
    $iDice6 = mt_rand(0,5);

    //dice values assigned
    $player1Dice1 = $diceValues[$iDice1];
    $player1Dice2 = $diceValues[$iDice2];
    $player1Dice3 = $diceValues[$iDice3];
    $player2Dice1 = $diceValues[$iDice4];
    $player2Dice2 = $diceValues[$iDice5];
    $player2Dice3 = $diceValues[$iDice6];

    /*$player1Dice = array();
    for($i=0;$i<=2;$i++){
        $player1Dice[$i] = $player1Dice{$i+1};
        $player1Dice[$i] = $diceValues[$iDice{$i+1}];
    }*/

    //player totals
    $player1Total = $player1Dice1+$player1Dice2+$player1Dice3;
    $player2Total = $player2Dice1+$player2Dice2+$player2Dice3;

    //Dice images
    $image1 = "/images/".$player1Dice1.".png";
    $image2 = "/images/".$player1Dice2.".png";
    $image3 = "/images/".$player1Dice3.".png";
    $image4 = "/images/".$player2Dice1.".png";
    $image5 = "/images/".$player2Dice2.".png";
    $image6 = "/images/".$player2Dice3.".png";

    /*for($i=1;$$i<=6;$i++){
        $image{$i} = "/images/"{$player1Dice;}{$i;}".png";
    }*/
?>

<!doctype html>
<html language="en">
<head>
    <meta charset="UTF-8">
    <title>Taylor's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h2>Dice Game</h2>
    <br />
        <p>Player 1 Total: <b><?=$player1Total?></b></p>
    <div class="row" style="margin-left: 33.33%">
        <div class="column">
            <img src="<?php echo $image1?>" alt="<?php echo $player1Dice1; ?>" style="width: 56.5px;height: 56px"/>
        </div>
        <div class="column">
            <img src="<?php echo $image2?>" alt="<?php echo $player1Dice2; ?>" style="width: 56.5px;height: 56px"/>
        </div>
        <div class="column">
            <img src="<?php echo $image3?>" alt="<?php echo $player1Dice3; ?>" style="width: 56.5px;height: 56px"/>
        </div>
    </div>
    <br />
        <p>Player 2 Total: <b><?=$player2Total?></b></p>
    <div class="row" style="margin-left: 33.33%">
        <div class="column">
            <img src="<?php echo $image4?>" alt="<?php echo $player2Dice1; ?>" style="width: 56.5px;height: 56px"/>
        </div>
        <div class="column">
            <img src="<?php echo $image5?>" alt="<?php echo $player2Dice2; ?>" style="width: 56.5px;height: 56px"/>
        </div>
        <div class="column">
            <img src="<?php echo $image6?>" alt="<?php echo $player2Dice3; ?>" style="width: 56.5px;height: 56px"/>
        </div>
    </div>
    <br />
    <form action="dice.php" style="margin: 15px;">
        <input type="submit" value="Re-roll the dice">
    </form>

</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
