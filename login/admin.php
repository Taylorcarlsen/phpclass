<?php
session_start();
$errmsg="";
//create guid
$Key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));


if($_SESSION["Role"!=1]){
    header("Location:index.php");
}
if(isset($_POST["submit"])){
    if(empty($_POST["txtFName"])){
        $errmsg = "Full name is required";
    }else{
        $FName=$_POST["txtFName"];
    }
    if(empty($_POST["txtEmail"])){
        $errmsg = "Email is required";
    }else{
        $Email=$_POST["txtEmail"];
    }
    if(empty($_POST["txtPassword"])){
        $errmsg = "Password is required";
    }else{
        $pwd = $_POST["txtPassword"];
    }
    if($pwd != $_POST["txtPassword2"]){
        $errmsg = "Passwords do not match";
    }
    if(empty($_POST["txtRole"])){
        $errmsg = "Role is required";
    }else{
        $Role=$_POST["txtRole"];
    }
    if($errmsg==""){
        //Do db work
        //database stuff
        include '../includes/dbConn.php';
        try{
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("insert into memberLogin (memberName,memberEmail,memberPassword,roleID,memberKey) VALUE(:Name,:Email,:Password,:roleID,:Key)");
            $sql->bindValue(":Name",$FName);
            $sql->bindValue(":Email",$Email);
            $sql->bindValue(":Password",md5($pwd . $Key));
            $sql->bindValue(":roleID",$Role);
            $sql->bindValue(":Key",$Key);
            $sql->execute();
        }catch (PDOException $e){
            $error = $e->getMessage();
            echo "Error: $error";
        }
        $FName = "";
        $Email = "";
        $pwd = "";
        $errmsg = "Member Added to Database";
    }
}

?>

<!doctype html>
<html language="en">
<head>
    <meta charset="UTF-8">
    <title>Taylor's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h1>Admin Page</h1>
    <h3 id="error"><?=$errmsg?></h3>
    <form method="post">
        <table border = "1" width = "80%"">
        <tr height = "50">
            <th colspan="2">Add New Member</th>
        </tr>
        <tr height = "40">
            <th>Full Name</th>
            <td><input type="text" size="40" id="txtFName" name="txtFName" value="<?=$FName?>" required></td>
        </tr>
        <tr height = "40">
            <th>Email</th>
            <td><input type="email" size="40" id="txtEmail" name="txtEmail" value="<?=$Email?>" required></td>
        </tr>
        <tr height = "40">
            <th>Password</th>
            <td><input type="password" size="40" id="txtPassword" name="txtPassword" value="<?=$pwd?>" required></td>
        </tr>
        <tr height = "40">
            <th>Retype Password</th>
            <td><input type="password" size="40" id="txtPassword2" name="txtPassword2" required></td>
        </tr>
        <tr height = "40">
            <th>Role</th>
            <td>
                <select id="txtRole" name="txtRole">
                    <?php
                    include '../includes/dbConn.php';

                    try{
                        $db = new PDO($dsn, $username, $password, $options);

                        $sql = $db->prepare("select * from role");
                        $sql->execute();
                        $roleID = $row["roleID"];
                        $roleValue = $row["roleValue"];
                        $row = $sql->fetch();

                        while ($row != null){
                            echo "<option value=" . $row["roleID"] . ">" . $row["roleValue"] . "</option>";
                            $row = $sql->fetch();
                        }
                    }catch (PDOException $e){
                        $error = $e->getMessage();
                        echo "Error: $error";
                    }
                    ?>

                </select>
            </td>
        </tr>
        <tr height = "50">
            <th colspan="2">
                <input type="submit" value="Add New Member" name="submit" id="submit">
            </th>
        </tr>
        </table>
    </form>
    <br />
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>