<?php
session_start();

if(isset($_POST["txtEmail"])) {
    if (isset($_POST["txtPassword"])) {
        $email = $_POST["txtEmail"];
        $pass = $_POST["txtPassword"];
        $errmsg = "";

        include '../includes/dbConn.php';

        try{
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("select memberID, memberPassword, memberKey, roleID from memberLogin where memberEmail = :Email");
            $sql->bindValue(":Email",$email);
            $sql->execute();
            $row = $sql->fetch();

            if($row != null){

                $hashedPassword = md5($pass . $row["memberKey"]);
                echo $hashedPassword;

                if($hashedPassword == $row["memberPassword"]){
                    $_SESSION["UID"] = $row["memberID"];
                    $_SESSION["Role"] = $row["roleID"];
                    if($row["roleID"] == 1){
                        header("Location:admin.php");
                    }else{
                        header("Location:member.php");
                    }
                }else{
                    $errmsg="Wrong username or password";
                }
            }else{
                $errmsg="Wrong username or password";
            }

        }catch (PDOException $e){
            $error = $e->getMessage();
            echo "Error: $error";
        }
    }
}

?>

<!doctype html>
<html language="en">
<head>
    <meta charset="UTF-8">
    <title>Taylor's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <form method="post">
        <h3 id="error"><?=$errmsg?></h3>
        <table border = "1" width = "80%"">
            <tr height = "50">
                <th colspan="2">User Login</th>
            </tr>
            <tr height = "40">
                <th>Email</th>
                <td><input type="text" size="40" id="txtEmail" name="txtEmail" ></td>
            </tr>
            <tr height = "40">
                <th>Password</th>
                <td><input type="password" size="40" id="txtPassword" name="txtPassword" ></td>
            </tr>
            <tr height = "50">
                <th colspan="2">
                    <input type="submit" value="Login">
                </th>
            </tr>
        </table>
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>