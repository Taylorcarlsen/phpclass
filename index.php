<!doctype html>
<html language="en">
<head>
    <meta charset="UTF-8">
    <title>Taylor's Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/base.css">
</head>
<body>
    <header><?php include 'includes/header.php' ?></header>
    <nav><?php include 'includes/nav.php' ?></nav>
    <main>
        <img src="images/taylor.jpg" alt="Picture of Taylor"/>
        <p>Hello my name is Taylor. I am currently enrolled at FVTC as a Software Development major. I enjoy going to the gym and expanding my practical knowledge when I can. I hope one day to consult and have my own side business in development.</p>
    </main>
    <footer><?php include 'includes/footer.php' ?></footer>
</body>
</html>