<!doctype html>
<html language="en">
<head>
    <meta charset="UTF-8">
    <title>Taylor's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <style>tr:nth-child(even){background-color: lavender;}</style>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h3>Customer List</h3>
    <table border="1" width="125%">
           <tr>
               <th>Customer ID</th>
               <th>First Name</th>
               <th>Last Name</th>
               <th>Address</th>
               <th>City</th>
               <th>State</th>
               <th>Zip</th>
               <th>Phone</th>
               <th>Email</th>
               <th>Password</th>
           </tr>
        <?php
            include '../includes/dbConn.php';

            try{
                $db = new PDO($dsn, $username, $password, $options);

                $sql = $db->prepare("select * from customerlist");
                $sql->execute();
                $row = $sql->fetch();

                while ($row != null){
                    echo "<tr>";
                    echo "<td>" . $row["CustomerID"] . "</td>";
                    echo "<td><a href=customerupdate.php?id=" . $row["CustomerID"] . ">" . $row["FirstName"] . "</a></td>";
                    echo "<td>" . $row["LastName"] . "</td>";
                    echo "<td>" . $row["Address"] . "</td>";
                    echo "<td>" . $row["City"] . "</td>";
                    echo "<td>" . $row["State"] . "</td>";
                    echo "<td>" . $row["Zip"] . "</td>";
                    echo "<td>" . $row["Phone"] . "</td>";
                    echo "<td>" . $row["Email"] . "</td>";
                    echo "<td>" . $row["Password"] . "</td>";

                    $row = $sql->fetch();
                }
            }catch (PDOException $e){
                $error = $e->getMessage();
                echo "Error: $error";
            }

        ?>
    </table>
    <br /><br />
    <a href="customeradd.php">Add New Customer</a>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>