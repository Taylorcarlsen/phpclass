<?php
//create guid
$key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

if(isset($_POST["txtFName"], $_POST["txtLName"], $_POST["txtAddress"], $_POST["txtCity"], $_POST["txtState"], $_POST["txtZip"], $_POST["txtPhone"], $_POST["txtEmail"], $_POST["txtPass"], $_POST["txtPass_confirm"])) {
    $fname = $_POST["txtFName"];
    $lname = $_POST["txtLName"];
    $address = $_POST["txtAddress"];
    $city = $_POST["txtCity"];
    $state = $_POST["txtState"];
    $zip = $_POST["txtZip"];
    $phone = $_POST["txtPhone"];
    $email = $_POST["txtEmail"];
    $pass = $_POST["txtPass"];
    $pass_confirm = $_POST["txtPass_confirm"];


    //database
    include '../includes/dbConn.php';

    try {
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("insert into customerlist (FirstName, LastName, Address, City, State, Zip, Phone, Email, Password, customerKey) VALUE(:FName,:LName,:Address,:City,:State,:Zip,:Phone,:Email,:Pass,:Key)");
        $sql->bindValue(":FName", $fname);
        $sql->bindValue(":LName", $lname);
        $sql->bindValue(":Address", $address);
        $sql->bindValue(":City", $city);
        $sql->bindValue(":State", $state);
        $sql->bindValue(":Zip", $zip);
        $sql->bindValue(":Phone", $phone);
        $sql->bindValue(":Email", $email);
        $sql->bindValue(":Pass", md5($pass . $key));
        $sql->bindValue(":Key", $key);
        $sql->execute();


    } catch (PDOException $e) {
        $error = $e->getMessage();
        echo "Error: $error";
    }
    header("Location:customerlist.php");
}
?>

<!doctype html>
<html language="en">
<head>
    <meta charset="UTF-8">
    <title>Taylor's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <script type="text/javascript">
        let check = function(){
            if (document.getElementById('txtPass').value ==
                document.getElementById('txtPass_confirm').value) {
                document.getElementById('message').style.color = 'green';
                document.getElementById('message').innerHTML = 'Matching';
            } else {
                document.getElementById('message').style.color = 'red';
                document.getElementById('message').innerHTML = 'Not Matching';
            }
        }
    </script>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h3>Create Account</h3>
    <form method="post">
    <fieldset>
        <legend align="left">Customer</legend>
        <table>
            <tr height="40">
                <th align="right">First Name:</th>
                <td><input type="text" size="40" id="txtFName" name="txtFName" required></td>
            </tr>
            <tr height="40">
                <th align="right">Last Name:</th>
                <td><input type="text" size="40" id="txtLName" name="txtLName" required></td>
            </tr>
            <tr height="40">
                <th align="right">Phone:</th>
                <td><input type="tel" size="40" id="txtPhone" name="txtPhone" placeholder="(555)555-5555" pattern="[0-9]{3}-[0-9]{3}-[0-9]{}" required></td>
            </tr>
            <tr height="40">
                <th align="right">Email:</th>
                <td><input type="email" size="40" id="txtEmail" name="txtEmail" placeholder="example@example.com" required></td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <legend align="left">Address</legend>
        <table>
            <tr>
                <th align="right">Address:</th>
                <td><input type="text" size="40" id="txtAddress" name="txtAddress" required></td>

            </tr>
            <tr height="40">
                <th align="right">City:</th>
                <td><input type="text" size="40" id="txtCity" name="txtCity" required></td>
            </tr>
            <tr height="40">
                <th align="right">Zip:</th>
                <td><input type="text" size="40" id="txtZip" name="txtZip" required></td>
            </tr>
            <tr heigh="40">
                <th align="right">State:</th>
                <td><select id="txtState" name="txtState" required>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="DC">District Of Columbia</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                    </select>	</td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <legend align="left">Security</legend>
        <table>
            <tr height="40">
                <th align="right">Password:</th>
                <td><input type="password" size="40" id="txtPass" name="txtPass" onkeyup="check();" required></td>
            </tr>
            <tr height="40">
                <th align="right">Re-Type Password:</th>
                <td><input type="password" size="40" id="txtPass_confirm" name="txtPass_confirm" placeholder="re-enter password" onkeyup="check();" required></td>
            </tr>
            <tr height="40">
                <th align="right">Validation:<span id = 'message'></span></th>
            </tr>
        </table>
    </fieldset>
    <br />
        <input type="submit" value="Submit" style="font-size: medium"><button style="margin-left: 20px; font-size: medium" type="reset">Reset</button>
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>