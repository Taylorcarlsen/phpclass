<?php
//create guid
$key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

include '../includes/dbConn.php';

if(isset($_POST["txtFName"], $_POST["txtLName"], $_POST["txtAddress"], $_POST["txtCity"], $_POST["txtState"], $_POST["txtZip"], $_POST["txtPhone"], $_POST["txtEmail"],$_POST["txtPass"], $_POST["txtPass_confirm"])) {

    $fname = $_POST["txtFName"];
    $lname = $_POST["txtLName"];
    $address = $_POST["txtAddress"];
    $city = $_POST["txtCity"];
    $state = $_POST["txtState"];
    $zip = $_POST["txtZip"];
    $phone = $_POST["txtPhone"];
    $email = $_POST["txtEmail"];
    $pass = $_POST["txtPass"];
    $pass_confirm = $_POST["txtPass_confirm"];
    $id = $_POST["txtID"];
    $memberKey = $_POST["txtKey"];

    /*if (is_null($_POST["txtKey"])) {
        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("update customerlist set customerKey = :Key where CustomerID = :ID");
            $sql->bindValue(":Key", $key);
            $sql->bindValue(":ID", $id);
        } catch (PDOException $e) {
            $error = $e->getMessage();
            echo "Error: $error";
        }
    } elseif (empty($_POST["txtKey"])) {
        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("update customerlist set customerKey = :Key where CustomerID = :ID");
            $sql->bindValue(":Key", $key);
            $sql->bindValue(":ID", $id);
        } catch (PDOException $e) {
            $error = $e->getMessage();
            echo "Error: $error";
        }
    } else {*/
        //database

        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("update customerlist set FirstName = :FName, LastName = :LName, Address = :Address, City = :City, State = :State, Zip = :Zip, Phone = :Phone, Email = :Email, Password = :Pass where CustomerID = :ID");
            $sql->bindValue(":FName", $fname);
            $sql->bindValue(":LName", $lname);
            $sql->bindValue(":Address", $address);
            $sql->bindValue(":City", $city);
            $sql->bindValue(":State", $state);
            $sql->bindValue(":Zip", $zip);
            $sql->bindValue(":Phone", $phone);
            $sql->bindValue(":Email", $email);
            $sql->bindValue(":Pass", md5($pass . $memberKey));
            $sql->bindValue(":ID", $id);
            $sql->execute();
            header("Location:customerlist.php");
        } catch (PDOException $e) {
            $error = $e->getMessage();
            echo "Error: $error";
        }
    }
//}
if(isset($_GET["id"])){
    $id=$_GET["id"];
    try{
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("select * from customerlist where CustomerID = :id");
        $sql->bindValue(":id",$id);
        $sql->execute();
        $row = $sql->fetch();               //fetch values from the db
        $fname = $row["FirstName"];      //put values into the UI table from the db
        $lname = $row["LastName"];
        $address = $row["Address"];
        $city = $row["City"];
        $state = $row["State"];
        $zip = $row["Zip"];
        $phone = $row["Phone"];
        $email = $row["Email"];
        //$pass = $row["Password"];
    }catch (PDOException $e){
        $error = $e->getMessage();
        echo "Error: $error";
    }
}else{
    header("Location:customerlist.php");
}
?>

<!doctype html>
<html language="en">
<head>
    <meta charset="UTF-8">
    <title>Taylor's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <script type="text/javascript">
        let check = function(){
            if (document.getElementById('txtPass').value ==
                document.getElementById('txtPass_confirm').value) {
                document.getElementById('message').style.color = 'green';
                document.getElementById('message').innerHTML = 'Matching';
            } else {
                document.getElementById('message').style.color = 'red';
                document.getElementById('message').innerHTML = 'Not Matching';
            }
        }
    </script>
    <script type="text/javascript">
        function DeleteCustomer(lname,fname,id) {
            if(confirm("Do you want to delete: " + fname + " " + lname)){
                document.location.href = "customerdelete.php?id=" + id;
            }
        }
    </script>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h3>Update Account</h3>
    <form method="post">
    <fieldset>
        <legend align="left">Customer</legend>
        <table>
            <tr height="40">
                <th align="right">First Name:</th>
                <td><input type="text" size="40" id="txtFName" name="txtFName" value="<?=$fname?>" required></td>
            </tr>
            <tr height="40">
                <th align="right">Last Name:</th>
                <td><input type="text" size="40" id="txtLName" name="txtLName" required value="<?=$lname?>"></td>
            </tr>
            <tr height="40">
                <th align="right">Phone:</th>
                <td><input type="tel" size="40" id="txtPhone" name="txtPhone" placeholder="(555)555-5555" pattern="[0-9]{3}-[0-9]{3}-[0-9]{}" value="<?=$phone?>" required></td>
            </tr>
            <tr height="40">
                <th align="right">Email:</th>
                <td><input type="email" size="40" id="txtEmail" name="txtEmail" placeholder="example@example.com" value="<?=$email?>" required></td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <legend align="left">Address</legend>
        <table>
            <tr>
                <th align="right">Address:</th>
                <td><input type="text" size="40" id="txtAddress" name="txtAddress" value="<?=$address?>" required></td>

            </tr>
            <tr height="40">
                <th align="right">City:</th>
                <td><input type="text" size="40" id="txtCity" name="txtCity" value="<?=$city?>" required></td>
            </tr>
            <tr height="40">
                <th align="right">Zip:</th>
                <td><input type="text" size="40" id="txtZip" name="txtZip" value="<?=$zip?>" required></td>
            </tr>
            <tr heigh="40">
                <th align="right">State:</th>
                <td><select id="txtState" name="txtState" value="<?=$state?>" required>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="DC">District Of Columbia</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                    </select>	</td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <legend align="left">Security</legend>
        <table>
            <tr height="40">
                <th align="right">Password:</th>
                <td><input type="password" size="40" id="txtPass" name="txtPass" onkeyup="check();" value="<?=$pass?>" required></td>
            </tr>
            <tr height="40">
                <th align="right">Re-Type Password:</th>
                <td><input type="password" size="40" id="txtPass_confirm" name="txtPass_confirm" placeholder="re-enter password" onkeyup="check();" value="<?=$pass_confirm?>" required></td>
            </tr>
            <tr height="40">
                <th align="right">Validation:<span id = 'message'></span></th>
            </tr>

        </table>
    </fieldset>
    <br />
        <input type="submit" value="Update" style="font-size: medium"> | <input type="button" onclick="DeleteCustomer('<?=$lname?>','<?=$fname?>','<?=$id?>')" value="Delete" style="font-size: medium">
        <input type="hidden" id="txtID" name="txtID" value="<?=$id?>">
    </form>

</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>