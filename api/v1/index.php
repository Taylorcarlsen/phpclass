<?php
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

/* put = update
 * post = inset
 * get = select
 * delete = Delete
*/

$app = new \Slim\Slim();

/*$app->get('/getHello', 'getHello');
$app->get('/showMember/:MemberName', 'showMember');
$app->post('/addMember/:MemberName', 'addMember');
$app->delete('/delUser/:userID', 'delUser');*/
$app->post('/addJson/', 'addJson');

$app->get('/getRaces', 'get_races');
$app->get('/getRunners/:RaceID', 'get_runners');
$app->post('/addRunner', 'add_runner');
$app->delete('/delRunner/:MemberID', 'del_runner');
$app->run();

function getHello(){
    echo "Hello World";
}
function showMember($MemberName){
    echo "Hello $MemberName";
}

function addMember($MemberName){
    echo "Hello new member $MemberName";
}

function delUser($userID)
{
    echo "User: $userID was deleted.";
}
function addJson(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(),TRUE);

    echo $post_json["address"];
}

function get_races(){
    include '../../includes/dbConn.php';

    try{
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("select * from race");
        $sql->execute();
        $results = $sql->fetchAll();
        echo '{"Races":' . json_encode($results) . '}';
        $results = null;
        $db = null;
    }catch (PDOException $e){
        $error = $e->getMessage();
        echo json_encode($error);
    }
}

function get_runners($RaceID){
    include '../../includes/dbConn.php';

    try{
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("SELECT DISTINCT memberLogin.memberName, memberLogin.memberEmail FROM memberLogin
        INNER JOIN member_race ON memberLogin.memberID = member_race.memberID WHERE member_race.raceID = :raceID");
        $sql ->bindValue(":raceID",$RaceID);
        $sql->execute();
        $results = $sql->fetchAll();
        echo '{"Runners":' . json_encode($results) . '}';
        $results = null;
        $db = null;
    }catch (PDOException $e){
        $error = $e->getMessage();
        echo json_encode($error);
    }
}

function add_runner(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(),TRUE);
    $memberID = $post_json["memberID"];
    $raceID = $post_json["raceID"];
    $memberKey = $post_json["memberKey"];
    include '../../includes/dbConn.php';

    try{
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("SELECT member_race.raceID FROM member_race INNER JOIN memberLogin ON member_race.memberID = memberLogin.memberID WHERE member_race.raceID = 2 AND member_race.roleID = 2 AND memberLogin.memberKey = :APIKey");
        $sql ->bindValue(":APIKey",$memberKey);
        $sql->execute();
        $results = $sql->fetch();

        if($results == null){
            echo "Bad API Key";
        }else{
            $sql = $db->prepare("INSERT INTO member_race(memberID,raceID,roleID) values(:memberID,:raceID,3)");
            $sql ->bindValue(":memberID",$memberID);
            $sql ->bindValue(":raceID",$raceID);
            $sql->execute();
        }
        $results = null;
        $db = null;
    }catch (PDOException $e){
        $error = $e->getMessage();
        echo json_encode($error);
    }
}

function del_runner($memberID){
    include '../../includes/dbConn.php';

    try{
        $db = new PDO($dsn, $username, $password, $options);

        $sql = $db->prepare("SELECT * FROM member_race WHERE memberID = :MemberID");
        $sql->bindValue(":MemberID",$memberID);
        $sql->execute();
        $results = $sql->fetch();

        if($results!=null)
        {
            $sql = $db->prepare("delete from member_race where memberID = :MemberID");
            $sql ->bindValue(":MemberID",$memberID);
            $sql->execute();
        }
        $sql = $db->prepare("SELECT memberID, memberKey FROM memberLogin where memberID = :MemberID");
        $sql -> bindValue(":MemberID", $memberID);
        $sql->execute();
        $results = $sql->fetch();

        if($results == null){
            echo "Member does not exist.";
        }else{
            $sql = $db->prepare("delete from memberLogin where memberID = :MemberID");
            $sql ->bindValue(":MemberID",$memberID);
            $sql->execute();
        }
        $results = null;
        $db = null;
    }catch (PDOException $e){
        $error = $e->getMessage();
        echo json_encode($error);
    }
}
?>